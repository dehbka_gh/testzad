import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        idCategory: 1,
        photos: [],
        totalItems: 0,
        totalPages: 25,
        currentPage: 1,
        currentPhotoName: '',
        currentPhotoDescription: '',
        currentPhotoImage: '',
        isWindow: false
    },
    getters: {
        PHOTOS: state => {
            return state.photos
        },
        TOTAL_PAGES: state => {
            return state.totalPages
        },
        TOTAL_ITEMS: state => {
            return state.totalItems
        },
        CURRENT_PAGE: state => {
            return state.currentPage
        },
        CURRENT_PHOTO: state => {
            return state.photos.id
        },
        IS_WINDOW: state => {
            return state.isWindow
        }
    },
    mutations: {
        SET_IMAGES: (state, payload) => {
            state.photos = payload.data;
            state.totalItems = payload.totalItems;
            state.totalPages = payload.countOfPages;
        },
        SET_CATEGORY: (state, payload) => {
            state.idCategory = payload
            state.currentPage = 1;
        },
        PREV_PAGE: (state) => {
            state.currentPage--;
        },
        NEXT_PAGE: (state) => {
            state.currentPage++;
        },
        CHANGE_PAGE: (state, payload) => {
            state.currentPage = payload;
        },
        CHANGE_WINDOW: (state, payload) => {
            state.currentPhotoName = payload.name;
            state.currentPhotoDescription = payload.description;
            state.currentPhotoImage = payload.src;
            state.isWindow = payload.isWindow;
        }
    },
    actions: {
        GET_IMAGES: async (context, payload) => {
            let {data} = await axios.get('/api/photos', {
                params: {
                    page: context.state.currentPage,
                    new: (context.state.idCategory == 1) ? true : false,
                    popular: (context.state.idCategory == 1) ? false : true,
                    limit: 10
                }
            })
            console.log(data);
            context.commit('SET_IMAGES', data);
        },
        CHANGE_CATEGORY: (context, payload) => {
            context.commit('SET_CATEGORY', payload);
        },
        PREV_PAGE: (context, payload) => {
            context.commit('PREV_PAGE');
        },
        NEXT_PAGE: (context, payload) => {
            context.commit('NEXT_PAGE');
        },
        CHANGE_PAGE: (context, payload) => {
            context.commit('CHANGE_PAGE', payload);
        },
        CHANGE_WINDOW: (context, payload) => {
            context.commit('CHANGE_WINDOW', payload);
        }
    }
});