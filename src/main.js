import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import './assets/element-variables.scss'
import Gallery from './components/Gallery.vue'
import HeadMenu from './components/HeadMenu.vue'
import ShowPhoto from './components/ShowPhoto.vue'
import {store} from './store';

Vue.component('gallery', Gallery);
Vue.component('head-menu', HeadMenu);
Vue.component('show-photo', ShowPhoto);

Vue.use(VueRouter);
Vue.use(ElementUI);

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})
